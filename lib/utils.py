def validate_list_of_strings(strings):
    """Validates a list is a list of strings

        Args:
            strings (:type: list of str): List of strings

        Raises:
            ValueError - One element of list is not a string
    """
    for string in strings:
        if not isinstance(string, str):
            raise ValueError

def validate_list_of_ints(ints):
    """Validates a list is a list of ints

        Args:
            ints (:type: list of int): List of ints

        Raises:
            ValueError - One element of list is not an integer
    """
    for i in ints:
        if not isinstance(i, int):
            raise ValueError
