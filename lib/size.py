from constants import *
from utils import *
from interface import *

class Size(object):
    """Size Object

        Args:
            **kwargs: Arbitrary keyword arguments.

        Attributes:
            slug (str): A human-readable string that is used to uniquely identify each size.
            memory (int): The amount of RAM allocated to Droplets created of this size. The value is represented in megabytes.
            vcpus (int): The integer of number CPUs allocated to Droplets of this size.
            disk (int): The amount of disk space set aside for Droplets of this size. The value is represented in gigabytes.
            transfer (float): The amount of transfer bandwidth that is available for Droplets created in this size. This only counts traffic on the public interface. The value is given in terabytes.
            price_monthly (float): This attribute describes the monthly cost of this Droplet size if the Droplet is kept for an entire month. The value is measured in US dollars.
            price_hourly (float): This describes the price of the Droplet size as measured hourly. The value is measured in US dollars.
            regions (:type: list of str): An array containing the region slugs where this size is available for Droplet creates.
            available (bool): This is a boolean value that represents whether new Droplets can be created with this size.
            description (str): A string describing the class of Droplets created from this size. For example: Basic, General Purpose, CPU-Optimized, Memory-Optimized, or Storage-Optimized.

        Raises:
            ValueError - Invalid arguments
    """
    def __init__(self, **kwargs):
        #
        # Required Args
        try:
            if not isinstance(kwargs['slug'], str):
                raise ValueError("Slug must be a string")

            self.__slug = kwargs['slug']
        except KeyError:
            raise ValueError("Slug is a required argument")

        #XXX better validation here
        try:
            if not isinstance(kwargs['memory'], int) or kwargs['memory'] < 8:
                raise ValueError("Memory must be an integer >= 8")

            self.__memory = kwargs['memory']
        except KeyError:
            raise ValueError("Memory is a required argument")

        try:
            if not isinstance(kwargs['vcpus'], int) or kwargs['memory'] <1:
                raise ValueError("vcpus must be an integer > 0")

            self.__vcpus = kwargs['vcpus']
        except KeyError:
            raise ValueError("Vcpus is a required argument")


        try:
            if not isinstance(kwargs['disk'], int) or kwargs['disk'] < 0:
                raise ValueError("disk must be a positive integer: {}".format(kwargs['disk']))

            self.__disk = kwargs['disk']
        except KeyError:
            raise ValueError("Disk is a required argument")

        try:
            if not isinstance(kwargs['transfer'], float) or kwargs['transfer'] < 0:
                raise ValueError("transfer must a float >= 1: {}".format(kwargs['transfer']))
            self.__transfer = kwargs['transfer']
        except KeyError:
            raise ValueError("Transfer is a required argument")

        try:
            if kwargs['price_monthly'] < 0:
                raise ValueError("price_monthly must be a float > 0")

            self.__price_monthly = kwargs['price_monthly']
        except KeyError:
            raise ValueError("price_monthly is a required argument")

        try:
            if not isinstance(kwargs['price_hourly'], float) or kwargs['price_hourly'] < 0:
                raise ValueError("price_hourly must be a float > 0")

            self.__price_hourly = kwargs['price_hourly']
        except KeyError:
            raise ValueError("price_hourly is a required argument")

        try:
            validate_list_of_strings(kwargs['regions'])
        except KeyError:
            raise ValueError("Region is a required argument")
        except ValueError:
            raise ValueError("Regions must be a list of strings")

        self.__regions = kwargs['regions']

        try:
            if not isinstance(kwargs['available'], bool):
                raise ValueError("available must be a boolean")

            self.__available = kwargs['available']
        except KeyError:
            raise ValueError("avaiilable is a requied argument")

        try:
            if not isinstance(kwargs['description'], str):
                raise ValueError("description must be a string")

            self.__description = kwargs['description']
        except KeyError:
            raise ValueError("description is a required argument")


    def slug(self):
        """Size Slug Accessor

            Returns:
                slug (str): Size Slug
        """
        return self.__slug

    def memory(self):
        """Size Memory Accessor

            Returns:
                memory (int): Size Memory
        """
        return self.__memory

    def vcpus(self):
        """Size VCPUs Accessor

            Returns:
                vcpus (int): Size VCPUs
        """
        return self.__vcpus

    def disk(self):
        """Size Disk Accessor

            Returns:
                disk (int): Size Disk
        """
        return self.__disk

    def transfer(self):
        """Size Transfer

            Returns:
                transfer (float): Size float
        """
        return self.__transfer

    def price_monthly(self):
        """Size Price Montly Accessor
            
            Returns:
                price_montly (float): Size Price Monthly
        """
        return self.__price_monthly

    def price_hourly(self):
        """Size Price Hourly Accessor

            Returns:
                price_hourly (float): Size price hourly
        """
        return self.__price_hourly

    def regions(self):
        """Size Regions Accessor

            Returns:
                regions (:type: list of str): List of regions for size
        """
        return self.__regions

    def available(self):
        """Size Available Accessor

            Returns:
                available (bool): Size availableA
        """
        return self.__available

    def description(self):
        """Size Description Accessor

            Returns:
                description (str): Size description
        """
        return self.__description

    @classmethod
    def list_sizes(cls):
        """List Sizes

            Returns:
                sizes (:type: list of :obj: Size): List of sizes
    
            Raises:
                ValueError - Invalid Size
                NotAuthorized - Not Authorized to list sizes
                TooManyRequests - Too many requests made
                InternalError - Internal Error
        """        
        ret = send_request("GET", SIZES_URL)
 
        sizes = []
        for size in ret['sizes']:
            sizes.append(Size(**size))
    
        return sizes
