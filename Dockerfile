FROM python:3

RUN pip3 install requests && \
    mkdir -p /usr/CAM/app && \
    mkdir -p /uar/CAM/lib

COPY lib/ /usr/CAM/lib

ENV PYTHONPATH /usr/CAM/lib
