#!/usr/local/bin/python3

import unittest

from exceptions import *
from droplet import Droplet
from window import Window
from image import Image
from size import Size
from networks import Networks
from networks.v4 import V4
from networks.v6 import V6
from region import Region

class TestDroplet(unittest.TestCase):

    def test_valid_creation(self):

        args = {
            "id": 3164444,
            "name": "example.com",
            "memory": 1024,
            "vcpus": 1,
            "disk": 25,
            "locked": False,
            "status": "active",
            "created_at": "2020-07-21T18:37:44Z",
            "features": [
                "backups",
                "private_networking",
                "ipv6"
            ],
            "backup_ids": [
                53893572
            ],
            "next_backup_window": {
                "start": "2020-07-30T00:00:00Z",
                "end": "2020-07-30T23:00:00Z"
            },
            "snapshot_ids": [
                67512819
            ],
            "image": {
                "id": 63663980,
                "name": "20.04 (LTS) x64",
                "distribution": "Ubuntu",
                "slug": "ubuntu-20-04-x64",
                "public": True,
                "regions": [
                    "ams2",
                    "ams3",
                    "blr1",
                    "fra1",
                    "lon1",
                    "nyc1",
                    "nyc2",
                    "nyc3",
                    "sfo1",
                    "sfo2",
                    "sfo3",
                    "sgp1",
                    "tor1"
                ],
                "created_at": "2020-05-15T05:47:50Z",
                "type": "snapshot",
                "min_disk_size": 20,
                "size_gigabytes": 2.36,
                "description": "",
                "tags": [ ],
                "status": "available",
                "error_message": ""
            },
            "volume_ids": [ ],
            "size": {
                "slug": "s-1vcpu-1gb",
                "memory": 1024,
                "vcpus": 1,
                "disk": 25,
                "transfer": 1.0,
                "price_monthly": 5,
                "price_hourly": 0.00743999984115362,
                "regions": [],
                "available": True,
                "description": "Basic"
            },
            "size_slug": "s-1vcpu-1gb",
            "networks": {
                "v4": [
                    {
                        "ip_address": "10.128.192.124",
                        "netmask": "255.255.0.0",
                        "gateway": None,
                        "type": "private"
                    },

                ],
                "v6": [
                    {
                        "ip_address": "2604:a880:0:1010::18a:a001",
                        "netmask": 64,
                        "gateway": "2604:a880:0:1010::1",
                        "type": "public"
                    }
                ]
            },
            "region": {
                "name": "New York 3",
                "slug": "nyc3",
                "features": [
                    "private_networking",
                    "backups",
                    "ipv6",
                    "metadata",
                    "install_agent",
                    "storage",
                    "image_transfer"
                ],
                "available": True,
                "sizes": []
            },
            "tags": [
                "web",
                "env:prod"
            ],
            "vpc_uuid": "760e09ef-dc84-11e8-981e-3cfdfeaae000"
        }

        drop = Droplet(**args)
        self.assertTrue(isinstance(drop, Droplet))
        self.assertEqual(drop.id(), 3164444)
        self.assertEqual(drop.name(), 'example.com')
        self.assertEqual(drop.memory(), 1024)
        self.assertEqual(drop.disk(), 25)
        self.assertFalse(drop.locked())
        self.assertEqual(drop.status(), 'active')
        self.assertEqual(drop.created_at(), "2020-07-21T18:37:44Z")

if __name__ == '__main__':
    unittest.main(buffer=False)
