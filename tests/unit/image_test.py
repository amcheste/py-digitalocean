#!/usr/local/bin/python3

import unittest

from exceptions import *
from image import Image

class TestImage(unittest.TestCase):

    def test_valid_creation(self):
        args = {
            'name': "14.04 x64",
            'distribution': "Ubuntu",
            'slug': "ubuntu-16-04-x64",
            'public': True,
            'regions': [
                "nyc1",
                "ams1",
                "sfo1"
            ]
        }

        img = Image(**args)
        self.assertTrue(isinstance(img, Image))
        self.assertEqual(img.name(), "14.04 x64")
        self.assertEqual(img.distribution(), "Ubuntu")
        self.assertEqual(img.slug(), "ubuntu-16-04-x64")
        self.assertEqual(img.public(), True)
        self.assertEqual(len(img.regions()), 3)
        self.assertEqual(img.regions()[0], 'nyc1')


    def test_valid_return(self):

        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }

        img = Image(**args)
        self.assertTrue(isinstance(img, Image))
        self.assertEqual(img.id(), 6918990)
        self.assertEqual(img.name(), "14.04 x64")
        self.assertEqual(img.distribution(), "Ubuntu")
        self.assertEqual(img.slug(), "ubuntu-16-04-x64")
        self.assertEqual(img.public(), True)
        self.assertEqual(len(img.regions()), 10)
        self.assertEqual(img.regions()[0], 'nyc1')
        self.assertEqual(img.created_at(), "2014-10-17T20:24:33Z")
        self.assertEqual(img.min_disk_size(), 20)
        self.assertEqual(img.size_gigabytes(), 2.34)
        self.assertEqual(img.description(), "")
        self.assertEqual(len(img.tags()), 0)
     
    def test_invalid_name(self):
        args = {
            "id": 6918990,
            "name": 123,
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_distribution(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "invalid",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_public(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": "blah",
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_regions(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                123,
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_id(self):
        args = {
            "id": "6918990",
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_type(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "type": "invalid",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_slug(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": 123,
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_created_at(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": 123,
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_min_disk_size(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": "20",
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_size_gigabyte(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": "2.34",
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_description(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": 123,
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_invalid_tags(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": ["abc", 1, "def"],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_missing_name(self):
        args = {
            "id": 6918990,
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_missing_distribution(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_missing_public(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "regions": [
                "nyc1",
                "ams1",
                "sfo1",
                "nyc2",
                "ams2",
                "sgp1",
                "lon1",
                "nyc3",
                "ams3",
                "nyc3"
            ],
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

    def test_missing_regions(self):
        args = {
            "id": 6918990,
            "name": "14.04 x64",
            "distribution": "Ubuntu",
            "slug": "ubuntu-16-04-x64",
            "public": True,
            "created_at": "2014-10-17T20:24:33Z",
            "min_disk_size": 20,
            "size_gigabytes": 2.34,
            "description": "",
            "tags": [],
        }
        with self.assertRaises(ValueError):
            img = Image(**args)

if __name__ == '__main__':
    unittest.main(buffer=False)
