#!/usr/local/bin/python3

import unittest

from exceptions import *
from networks import Networks
from networks.v4 import V4
from networks.v6 import V6

class TestNetworks(unittest.TestCase):
    

    

    def test_valid_creation(self):

        data = {
            "v4": [
                {
                    "ip_address": "10.128.192.124",
                    "netmask": "255.255.0.0",
                    "gateway": None,
                    "type": "private"
                },

            ],
            "v6": [
                {
                    "ip_address": "2604:a880:0:1010::18a:a001",
                    "netmask": 64,
                    "gateway": "2604:a880:0:1010::1",
                    "type": "public"
                }
            ]
        }
        networks = Networks(**data)
        self.assertTrue(isinstance(networks, Networks))


if __name__ == '__main__':
    unittest.main(buffer=False)
